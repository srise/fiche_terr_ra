---
params:
  codes_insee_territoire: [""]
  nom_territoire: ""
  type: "ftd"
author: "`r auteur`"
title: "Fiche territoriale détaillée RA 2020 « `r params$nom_territoire` »"
pagetitle: "`r params$nom_territoire`"
date: "`r format(Sys.Date(), '%d %B %Y')`"
output:
  rmdformats::readthedown:
    includes:
      in_header: !expr file.path(repertoire_utilisateur, "header.html")
editor_options: 
  chunk_output_type: console
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding, output_dir = "resultats") })
---

<!-- Taille des titres, suppression des puces et ajout d'un point dans la 
numérotation de la table des matières,... -->
<style>
  html * {font-family:Marianne, Helvetica, sans-serif}
  h1 {font-size:1.6em}
  h1.title {font-size:2em; color:black}
  h2 {font-size:1.4em}
  h3 {font-size:1.2em}
  h4 {font-size:1em; font-weight:bold}
  h1, h2, h3, h4, h5, h6, legend {color:`r couleurs$rouge_sombre`}
  a:hover {color:#7D4E5B}
  a:visited {color:#7D4E5B}
  caption {color:black; font-weight:bold; border-bottom:solid 2px black}
  table {font-size:1em}
  tfoot {font-size:.8em; text-align:right}
  thead tr th {color:white !important;
    font-weight:bold  !important;
    background-color:`r couleurs$vert` !important;
    border-top:0 !important; 
    border-bottom:0 !important}
  td {vertical-align:top !important}
  th {vertical-align:bottom !important}
  th {text-align:center !important}
  tr {background-color:white !important}
  
  /* éléments rmarkdown */
  #sidebar {background: `r couleurs$rouge_clair`}
  #postamble {background:`r couleurs$rouge_clair`; border-top:solid 10px `r couleurs$rouge`}
  .title {text-align:center; color:`r couleurs$rouge_sombre`}
  .subtitle {color:`r couleurs$rouge_sombre`}
  #sidebar h2 {background-color:`r couleurs$rouge`}
  #sidebar *, #sidebar abbr {color:#333333 !important}
  #sidebar a:hover {background-color:`r couleurs$rouge`}
  #main a {color:#7D4E5B !important}
  .toc-section-number:after, .header-section-number:after {content:". "}
  div#TOC {clear:both; margin-top:3em}
  div#TOC li {list-style:none}
  
  /* tableaux gt */
  .gt_footnote {line-height:1em !important}
  
  /* spécifique appli */
  .date {float:right}
  div#TOC a[href="#annexe-sources"]+ul {display:none;}
  .note_source p {font-size:.8em; text-align:right}
  .note_discrete {text-align:right; color:#ddd !important}
  .saut_de_page_avant {page-break-before:always}
  .retour_sommaire {text-align:right; color:#ddd !important}
  abbr[title] {border-bottom:0 !important}

  @media print {
    body {margin-top: 5mm; margin-bottom: 5mm; 
          margin-left: 0mm; margin-right: 0mm ; width:100%}
    h1 {page-break-before:always !important}
    h1.title {page-break-before:avoid !important}
    #content {width:100%}
    .retour_sommaire, #sidebar, #nav-top, #annexe-donnees h2, #annexe-donnees a {
      display:none !important}
    abbr[title]:after {content:""}
  }
  @page {
    margin-top: .5cm;
    margin-bottom: .5cm;
  }
</style>


```{r setup, include = FALSE}
# auteur, BD, etc.
source("parametres.R", encoding = "UTF-8")

if (length(params$codes_insee_territoire) == 0) {
  stop("Pas de filtre territoire. Il faut fournir une liste de codes INSEE de communes", call. = FALSE)
}

# ajout d'en-têtes HTML
write_lines(glue('<meta name="application" content="FTD_{version_ft}" />
                 <meta name="nodename" content="{Sys.info()["nodename"]}" />
                 <meta name="operator" content="{Sys.info()["effective_user"]}" />
                 <meta name="keywords" content="Agriculture, Recensement agricole, {params$nom_territoire}" />
                 <meta name="description" content="Fiche territoriale détaillée du recensement agricole - {params$nom_territoire}" />'),
            file.path(repertoire_utilisateur, "header.html"))

# options générales des chunks
opts_chunk$set(
  echo       = FALSE,
  warning    = FALSE,
  message    = FALSE,
  fig.height = 9,
  fig.width  = 9,
  dpi        = 92,
  # pour que le cache fonctionne et ne soit pas invalidé à chaque chgt de 
  # paramètre (territoire) :
  fig.path   = path.expand(path(repertoire_utilisateur, "figures_cache/html/")), 
  dev        = "png",
  dev.args   = list(type = "cairo-png")
) 

# dans le cas d'un doc HTML basique on ajoute des liens "retour sommaire" en fin de chaque chapitre
# sinon avec rmdformats::readthedown inutile
retour_sommaire <- if_else("html_document" %in% rmarkdown::all_output_formats(knitr::current_input()),
                           '<p class="retour_sommaire"><a href="#TOC">&larrhk; Sommaire</a></p>',
                           "")
```

```{r logo, results = "asis", eval = file.exists(here(fichier_logo))}
# insertion du logo avant l'auteur sous forme de data-URL, via javascript
cat(glue("<script>
  $('<img alt=\"\" src=\"{image_uri(here(fichier_logo))}\" style=\"width:100px;float:left;padding:2px 10px 10px 0\" />').insertBefore('.author')</script>"))
```

```{r logo2, results = "asis", eval = file.exists(here(fichier_entete_agreste))}
# insertion du bandeau Agreste sous forme de data-URL, via javascript
cat(glue("<script>
  $('<img alt=\"\" src=\"{image_uri(here(fichier_entete_agreste))}\" style=\"width:100%;float:left;padding:2px 0 10px 0\" />').insertBefore('.title')</script>"))
```

```{r donnees_communes, child = "donnees_communes.Rmd", eval = exec_chap(executer$donnees_communes)}
```

```{r post_donnees_communes, child = file.path(repertoire_utilisateur, "post_donnees_communes.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_donnees_communes.Rmd"))}
```

```{r, eval = filtre_population, results = "asis"}
cat(glue("# Population étudiée : {libelle_population}"))
```

```{r situation_geographique, child = "_situation_geographique.Rmd", eval = exec_chap(executer$situation_geographique)}
```

```{r post_situation_geographique, child = file.path(repertoire_utilisateur, "post_situation_geographique.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_situation_geographique.Rmd"))}
```

```{r chiffres_cles, child = "_chiffres_cles.Rmd", eval = exec_chap(executer$chiffres_cles)}
```

```{r chiffres_cles_ftd, child = "_chiffres_cles_ftd.Rmd", eval = exec_chap(executer$chiffres_cles_ftd)}
```

```{r post_chiffres_cles, child = file.path(repertoire_utilisateur, "post_chiffres_cles.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_chiffres_cles.Rmd"))}
```

```{r principaux_indicateurs, child = "_principaux_indicateurs.Rmd", eval = exec_chap(executer$principaux_indicateurs)}
```

```{r post_principaux_indicateurs, child = file.path(repertoire_utilisateur, "post_principaux_indicateurs.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_principaux_indicateurs.Rmd"))}
```

```{r main_doeuvre_succession, child = "_main_doeuvre_succession.Rmd", eval = exec_chap(executer$main_doeuvre_succession)}
```

```{r post_main_doeuvre_succession, child = file.path(repertoire_utilisateur, "post_main_doeuvre_succession.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_main_doeuvre_succession.Rmd"))}
```

```{r main_doeuvre_succession_ftd, child = "_main_doeuvre_succession_ftd.Rmd", eval = exec_chap(executer$main_doeuvre_succession_ftd)}
```

```{r post_main_doeuvre_succession_ftd, child = file.path(repertoire_utilisateur, "post_main_doeuvre_succession_ftd.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_main_doeuvre_succession_ftd.Rmd"))}
```

```{r prod_vegetales, child = "_prod_vegetales.Rmd", eval = exec_chap(executer$prod_vegetales)}
```

```{r post_prod_vegetales, child = file.path(repertoire_utilisateur, "post_prod_vegetales.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_prod_vegetales.Rmd"))}
```

```{r prod_vegetales_ftd, child = "_prod_vegetales_ftd.Rmd", eval = exec_chap(executer$prod_vegetales_ftd)}
```

```{r post_prod_vegetales_ftd, child = file.path(repertoire_utilisateur, "post_prod_vegetales_ftd.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_prod_vegetales_ftd.Rmd"))}
```

```{r irrigation, child = "_irrigation.Rmd", eval = exec_chap(executer$irrigation)}
```

```{r post_irrigation, child = file.path(repertoire_utilisateur, "post_irrigation.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_irrigation.Rmd"))}
```

```{r prod_animales, child = "_prod_animales.Rmd", eval = exec_chap(executer$prod_animales)}
```

```{r post_prod_animales, child = file.path(repertoire_utilisateur, "post_prod_animales.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_prod_animales.Rmd"))}
```

```{r prod_animales_ftd, child = "_prod_animales_ftd.Rmd", eval = exec_chap(executer$prod_animales_ftd)}
```

```{r post_prod_animales_ftd, child = file.path(repertoire_utilisateur, "post_prod_animales_ftd.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_prod_animales_ftd.Rmd"))}
```

```{r agri_bio_ftd, child = "_agri_bio_ftd.Rmd", eval = exec_chap(executer$agri_bio_ftd)}
```

```{r post_agri_bio_ftd, child = file.path(repertoire_utilisateur, "post_agri_bio_ftd.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_agri_bio_ftd.Rmd"))}
```

```{r siqo_div_cc, child = "_siqo_div_cc.Rmd", eval = exec_chap(executer$siqo_div_cc)}
```

```{r post_siqo_div_cc, child = file.path(repertoire_utilisateur, "post_siqo_div_cc.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_siqo_div_cc.Rmd"))}
```

```{r siqo_div_cc_ftd, child = "_siqo_div_cc_ftd.Rmd", eval = exec_chap(executer$siqo_div_cc_ftd)}
```

```{r post_siqo_div_cc_ftd, child = file.path(repertoire_utilisateur, "post_siqo_div_cc_ftd.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_siqo_div_cc_ftd.Rmd"))}
```

# Annexes {#annexes -}


```{r methodo, child = "_methodo.Rmd", eval = TRUE}
```

`r retour_sommaire`

```{r post_methodo, child = file.path(repertoire_utilisateur, "post_methodo.Rmd"), eval = file.exists(file.path(repertoire_utilisateur, "post_methodo.Rmd"))}
```


## Liste des communes {#annexe-liste-comm -}

```{r liste_comm, results = "asis"}
# NB : com_sig chargé dans chapitre_cadrage.Rmd
noms_com %>% 
  arrange(codgeo) %>% 
  glue_data("{libgeo} ({codgeo})") %>% 
  glue_collapse(sep = ", ", last = " et ")
  cat()
```

```{r export_tableur, eval = exec_chap(executer$export_tableur)}
creer_excel(liste_onglets,
            export_secretise,
            file.path(repertoire_destination,
                      glue("ftd_ra2020_{make_clean_names(nt)}.xlsx")))

creer_excel(liste_onglets,
            export,
            file.path(repertoire_destination,
                      glue("ftd_ra2020_{make_clean_names(nt)}_SANS_SECRET.xlsx")))
```

```{r telechargement_donnees_titre, results = "asis", eval = exec_chap(executer$telechargement_donnees)}
cat("## Télécharger les données {#annexe-donnees -}\n\n")
```

```{r telechargement_donnees, eval = exec_chap(executer$telechargement_donnees)}
download_file(
  path = file.path(repertoire_destination,
                   glue("ftd_ra2020_{make_clean_names(nt)}.xlsx")),
  output_name = "Fiche détaillée",
  button_label = "Tableur (*.xlsx)",
  button_type = "primary",
  has_icon = TRUE,
  self_contained = TRUE)
```

`r retour_sommaire`

::: {.note_discrete}
Version du générateur : FTD_`r version_ft`
:::
