# lancer après une mise à jour des fichiers R et Rmd sur CERISE pour les passer
# en lecture seule et éviter les modifications intempestives par les
# utilisateurs

system("chmod 744 *.R *.Rmd *.ini util/*.Rmd")

# sauf les logs qui doivent pouvoir être écrits par tous
system("chmod 777 util/*.sqlite")

# régénérer la documentation avec les bons chemins (de CERISE)
rmarkdown::render("documentation/guide_utilisateur.Rmd")
rmarkdown::render("documentation/guide_developpeur.Rmd")
