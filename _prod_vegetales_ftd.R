# Sript lié au chunk _prod_vegetale_ftd.Rmd 
# Fonctions de calcul de surfaces par groupe de culture, pour graphs Legumes et Ppam

#'`reclasser_surfaces`  agrege  surfaces par groupes de cultures et les petites surfaces dans catégorie 'autres"
# Calcule les surfaces agrégées (avec et sans secret)  sur les groupements donnés en param
#'@param surfaces df des données  brutes. Doit avoir champ <code > (char, code culture) et <surface> (numérique, surface cultivée)
#'@param libelles df de nomenclature de regrpement des varités dans des groupes culture (ceux à faire figurer sur le graph).
#' Doit avoir un champ <ligne> (code culture détaillé, char(3)), un champ <groupe> (code de regroupement, char(3), un champ <lib_groupe> char() )
#'@param nmax nb max de groupes culture à retenir, integer. Les groupes sont triés decroissant sur leur surface. Default = 6 groupes
#'@param pmin proportion minimale pour retenir un groupe culture, double. Default = 0.05 (5%)
#'@returns un df, avec surfaces et surfaces secretisées, sommées par groupe
#' df de la forme <classe>, <lib_classe>, <surf>, <surf_brut>, <part>
#'@examples
#' surfaces_legumes <- tibble::tribble(
#'   ~nom_dossier, ~legcode, ~legdev,
#'   "1567129","373",  20000,
#'   "1942265","373",   7000,
#'   "1384545","373",   5038,
#'   "1030986","310",  24400,
#'   "1739528","310",  7500,
#'   "1447661","310",  80000,
#'   "1241068","330",  4000,
#'   "1000303","330",  10000,
#'   "1303850","372",  9000,
#'   "1549222","372",  5480,
#'   "1987468","372",  33330,
#'   "1921241","090",  11000,
#'   "1776305","090",  49000,
#'   "1091551","090",  70000,
#'   "1516998","121",  33500,
#'   "1245400","121",  51038,
#'   "1838968","121",  44400)
#' 
#' libelles_leg <- tibble::tribble(
#'   ~ groupe, ~ligne, ~lib, ~lib_groupe,
#'   "090",	"090",	"Carotte",                                                             "Carotte", 
#'   "120",	"121",	"Chou blanc",                                                             "Chou", 
#'   "120",	"122",	"Chou de Bruxelles",                                                      "Chou", 
#'   "310",	"310",	"Poireau",                                                             "Poireau", 
#'   "330",	"330",	"Potiron, courge, giraumon, citrouille", "Potiron, courge, giraumon, citrouille", 
#'   "340",	"340",	"Radis",                                                        "Radis, raifort", 
#'   "370",	"372",	"Salade – laitue",                                                      "Salade", 
#'   "370",	"373",	"Salade – mâche",                                                       "Salade", 
#'   "370",	"374",	"Salade – cresson",                                                     "Salade", 
#'   "390",	"391",	"Tomates plein air",                                                   "Tomates", 
#'   "400",	"400",	"Légumes autres",                                               "Légumes autres", 
#'   "999",	"999",	"autres",                                                       "autres"         
#' )
#' surface_depart <- surfaces_legumes %>% left_join(libelles_leg, by = c("legcode"= "ligne"))
#' surfaces_reclassees <- reclasser_surfaces(surfaces = surfaces_legumes %>% select(nom_dossier, code = legcode, surface = legdev) , libelles = libelles_leg)
#'
reclasser_surfaces <- function(surfaces, libelles, nmax = 6, pmin = 0.05) {
  surfaces %>%
    select(nom_dossier, code, surface) %>%
    left_join(libelles, by = c("code" = "ligne")) %>%
    group_by(groupe, lib = lib_groupe, nom_dossier) %>%
    summarise(
      surface = sum(surface, na.rm = TRUE),
      .groups = "drop_last"
    ) %>%
    mutate(
      surface_groupe_brut = sum(surface * 1 / 10000, na.rm = TRUE),
      surface_groupe = secret_stat(surface, na.rm = TRUE, facteur = 1 / 10000),
    ) %>%
    ungroup() %>%
    arrange(desc(surface_groupe_brut)) %>%
    mutate(
      r = dense_rank(desc(surface_groupe_brut)),
      p = surface_groupe_brut / sum(surface * 1 / 10000 , na.rm = TRUE),
      part = 100 * p,
      classe = if_else(
        (r <= nmax & p >= pmin |
           r %in% c(2:3) & p >= 0.01 |
           r %in% c(4:5) & p >= 0.02 |
           r %in% c(6:8) & p >= 0.1
        ) & surface_groupe != -999,
        groupe, "999"
      ),
      lib_classe = if_else(classe == "999", "autres", lib)
    ) %>%
    group_by(classe, lib_classe, nom_dossier) %>%
    summarise(
      surface = sum(surface, na.rm = TRUE),
      .groups = "drop_last"
    ) %>%
    mutate(
      surface_classe_brut = secret_stat(surface, na.rm = TRUE, facteur = 1 / 10000),
      surface_classe = sum(surface * 1 / 10000, na.rm = TRUE),
      classe = ifelse(surface_classe == -999, 999, classe),
      lib_classe = ifelse(surface_classe == -999, autres, lib_classe),
    ) %>%
    group_by(classe, lib_classe) %>%
    summarise(
      surf = secret_stat(surface, na.rm = TRUE, facteur = 1 / 10000),
      surf_brut = sum(surface * 1 / 10000, na.rm = TRUE),
      .groups = "drop"
    ) %>%
    group_by(lib_classe, .drop = TRUE) %>%
    arrange(desc(surf_brut)) %>%
    filter(row_number() == 1) %>%
    ungroup() %>%
    mutate(
      part = 100 * surf_brut / sum(surf_brut, na.rm = TRUE)
    )
}

#'`calcule_surfaces_reclassees`  agrege  variable
# Calcule les surfaces agrégées (avec et sans secret)  sur les groupements donnés en param
#'@param surfaces df des données  brutes. Doit avoir champ <code > (char, code culture) et <surface> (numérique, surface cultivée)
#'@param classement df des données  des groupes culture (ceux à faire figurer sur le graph)
#'@examples 
#'
#' surfaces_legumes <- tibble::tribble(
#'   ~nom_dossier, ~legcode, ~legdev,
#'   "1567129","373",  20000,
#'   "1942265","373",   7000,
#'   "1384545","373",   5038,
#'   "1030986","310",  24400,
#'   "1739528","310",  7500,
#'   "1447661","310",  80000,
#'   "1241068","330",  4000,
#'   "1000303","330",  10000,
#'   "1303850","372",  9000,
#'   "1549222","372",  5480,
#'   "1987468","372",  33330,
#'   "1921241","090",  11000,
#'   "1776305","090",  49000,
#'   "1091551","090",  70000,
#'   "1516998","121",  33500,
#'   "1245400","121",  51038,
#'   "1838968","121",  44400)
#' 
#' libelles_leg <- tibble::tribble(
#'   ~ groupe, ~ligne, ~lib, ~lib_groupe,
#'   "090",	"090",	"Carotte",                                                             "Carotte", 
#'   "120",	"121",	"Chou blanc",                                                             "Chou", 
#'   "120",	"122",	"Chou de Bruxelles",                                                      "Chou", 
#'   "310",	"310",	"Poireau",                                                             "Poireau", 
#'   "330",	"330",	"Potiron, courge, giraumon, citrouille", "Potiron, courge, giraumon, citrouille", 
#'   "340",	"340",	"Radis",                                                        "Radis, raifort", 
#'   "370",	"372",	"Salade – laitue",                                                      "Salade", 
#'   "370",	"373",	"Salade – mâche",                                                       "Salade", 
#'   "370",	"374",	"Salade – cresson",                                                     "Salade", 
#'   "390",	"391",	"Tomates plein air",                                                   "Tomates", 
#'   "400",	"400",	"Légumes autres",                                               "Légumes autres", 
#'   "999",	"999",	"autres",                                                       "autres"         
#' )
#' 
#' codes_groupes_reclassees <- tibble::tribble(
#'   ~groupe, ~lib, ~classe, ~lib_classe, ~part,
#'   "090","Carotte","090","Carotte", 28.0,
#'   "120","Chou","120","Chou", 27.7,
#'   "310","Poireau","310","Poireau", 24.1,
#'   "370","Salade","370","Salade", 17.2,
#'   "330","Potiron, courge, giraumon, citrouille","999","autres", 3.01)
#' 
#' leg_2020_surf_groupes_reclassees <- calcule_surfaces_reclassees(leg_2020_terr_ %>%
#'                                                                   select(nom_dossier, code = legcode, surface = legdev),
#'                                                                 classement = codes_groupes_reclassees, libelles = libelles_leg)
#'
#'
calcule_surfaces_reclassees <- function(surfaces, classement, libelles) {
  surfaces %>%
    select(nom_dossier, code, surface) %>%
    left_join(libelles, by = c("code" = "ligne")) %>%
    left_join(classement %>% select(classe, lib_classe), by = c("groupe" = "classe"), keep = TRUE) %>%
    mutate(
      classe = ifelse(is.na(classe), "999", classe),
      lib_classe = ifelse(classe == "999", "autres", lib_classe)
    ) %>%
    group_by(classe, lib_classe, nom_dossier) %>%
    summarise(
      surface = sum(surface, na.rm = TRUE),
      .groups = "drop_last"
    ) %>%
    summarise(
      surf = secret_stat(surface, na.rm = TRUE, facteur = 1 / 10000),
      surf_brut = sum(surface * 1 / 10000, na.rm = TRUE),
      .groups = "drop"
    ) %>%
    mutate(
      part = surf_brut / sum(surf_brut, na.rm = TRUE),
    )
}

#'`classer_nouveau_groupe_dans_autres`  agrege   à la  classe  générique "autres" 
#'#'le code culture ayant la surface la plus petite  
#'@param classement df des données  des groupes culture (ceux à faire figurer sur le graph)
#'avec leur code groupe, leur classe (identique au groupe sauf pour les groupes de culture classés dans 'autres'
#
#'@return renvoie un df 
#' de la forme identique au param  
#'
classer_nouveau_groupe_dans_autres <- function(classement) {
  classement %>% 
    group_by(autre_ou_non = ifelse(classe == "999", 0, 1)) %>% 
    mutate(classe = ifelse(autre_ou_non == 1 & min_rank(part) ==1, "999", classe),
           lib_classe = ifelse(classe =="999", "autres", lib_classe)) %>%
    mutate(autre_ou_non = ifelse(classe == "999", 0, 1)) %>% 
    arrange(desc(autre_ou_non), desc(part)) %>%
    ungroup() %>% 
    select(-autre_ou_non)
}

#'`integrer_nouveau_groupe_a_autres`  agrege  les plus petites surface dans le groupe "autre" 
#'Regroupe les groupes de cultures à mettre sur le plot dans le cas où la classe 'autres'
#'est sous secret. 
#' Le regroupement se fait itérativement, tant que la classe "autres' est
#'sous secret statistique et tant qu'il reste au moins un groupe -pour avoir un graphe
#'avec "autres" et au moins un groupe culture-
#'@param forcer  Booleen Si True alors on on force le regroupemennt meme si pas de secret
#'@return df nouvelles surfaces avec le recalcul du groupe "autres" augmenté 
#' Modifie les objets codes_groupes_reclassees et leg_2020_surf_groupes_reclassees de l'envir. global
#'@examples
#' # data de départ
#' surfaces_legumes <- tibble::tribble(
#'   ~nom_dossier, ~legcode, ~legdev,
#'   "1567129","373",  20000,
#'   "1942265","373",   7000,
#'   "1384545","373",   5038,
#'   "1030986","310",  24400,
#'   "1739528","310",  7500,
#'   "1447661","310",  80000,
#'   "1241068","330",  4000,
#'   "1000303","330",  10000,
#'   "1303850","372",  9000,
#'   "1549222","372",  5480,
#'   "1987468","372",  33330,
#'   "1921241","090",  11000,
#'   "1776305","090",  49000,
#'   "1091551","090",  70000,
#'   "1516998","121",  33500,
#'   "1245400","121",  51038,
#'   "1838968","121",  44400)
#' 
#' # libellés avec regroupements (exemple salades diverses regroupées en 'salades')
#' libelles_leg <- tibble::tribble(
#'   ~ groupe, ~ligne, ~lib, ~lib_groupe,
#'   "090",	"090",	"Carotte",                                                             "Carotte", 
#'   "120",	"121",	"Chou blanc",                                                             "Chou", 
#'   "120",	"122",	"Chou de Bruxelles",                                                      "Chou", 
#'   "310",	"310",	"Poireau",                                                             "Poireau", 
#'   "330",	"330",	"Potiron, courge, giraumon, citrouille", "Potiron, courge, giraumon, citrouille", 
#'   "340",	"340",	"Radis",                                                        "Radis, raifort", 
#'   "370",	"372",	"Salade – laitue",                                                      "Salade", 
#'   "370",	"373",	"Salade – mâche",                                                       "Salade", 
#'   "370",	"374",	"Salade – cresson",                                                     "Salade", 
#'   "390",	"391",	"Tomates plein air",                                                   "Tomates", 
#'   "400",	"400",	"Légumes autres",                                               "Légumes autres", 
#'   "999",	"999",	"autres",                                                       "autres"         
#' )
#'  # classement apres regroupement en 'groupes' et apres tri decroissant des surfaces
#'  (voir \code{\link{reclasser_surfaces}})reclasser_surfaces
#'  # le groupe 'potiron' (code 330) a une petite surface (<5% des surfaces) et est classé dans "autres"
#'  classement_depart <- tibble::tribble(
#'    ~classe, ~lib_classe, ~surf, ~surf_brut, ~part,
#'    "090", "Carotte",      13,        13,    28.0, 
#'    "120", "Chou",         12.9,      12.9,  27.7, 
#'    "310", "Poireau",      11.2,      11.2,  24.1, 
#'    "370", "Salade",       7.98,      7.98, 17.2, 
#'    "999", "autres",     -999,        1.4,   3.01
#'    )
#' # Regroupement de la salade avec 'autre'
#' # en input : 'autre' est en secret
#' # en outpu:  'autre' n'est plus en secret et sa surface a augenté de celle de la salade (+7.98)
#'@examples
#' surfaces_legumes
#' classement_arrivee <- integrer_nouveau_groupe_a_autres(surfaces_groupes = classement_depart,
#' surfaces= surfaces_legumes %>% rename(code = legcode, surface = legdev),
#' libelles = libelles_leg)
#'
#'
integrer_nouveau_groupe_a_autres <- function(surfaces_groupes, surfaces, libelles, forcer = FALSE) {
  n_total <- surfaces_groupes %>%
    filter(classe != "999") %>%
    distinct(classe) %>%
    count() %>%
    as.integer()
  for (i in seq_along(1:n_total)) {
    autre_en_secret <- surfaces_groupes %>%
      mutate(autre_en_secret = ifelse(surf == "-999", TRUE, FALSE)) %>%
      filter(classe == "999") %>%
      select(autre_en_secret) %>%
      pull() %>%
      sum()
    n_restant <- surfaces_groupes %>%
      filter(classe != "999") %>%
      distinct(classe) %>%
      count() %>%
      as.integer()
    if (n_restant < 2) break
    if (autre_en_secret | forcer) {
      classement <- classer_nouveau_groupe_dans_autres(surfaces_groupes)
      surfaces_groupes <- calcule_surfaces_reclassees(
        surfaces %>%
          select(nom_dossier, code, surface),
        classement = classement, libelles = libelles
      )
    }
  }
  surfaces_groupes
}


#' `graphique_surfaces_detail` Plot  un df sous forme donut
#'@param df  df des données à ploter 
#'@return plot
#'@require colonne fraction -part cumulée de la varible, de 0->1,
#'@require ymin, ymax pour graph en coord polaires
#'@require lib_classe, couleur
#'@require surf_brut (pour afficher texte de la surface en ha sur le donut)
#'@example
#' surfaces <- tibble::tribble(
#'   ~classe, ~lib_classe, ~surf, ~surf_brut, ~fraction, ~ymax, ~ymin, ~ylabel, ~couleur,
#' "370","Salade"         , 366. , 366., 0.205,  0.205, 0,      0.102, "#7AB2D3",
#' "310","Poireau"        , 230. , 230., 0.128,  0.333, 0.205,  0.269, "#F8A160",
#' "120","Chou"           , 193.  , 193., 0.108,  0.441, 0.333,  0.387, "#227AB5",
#' "340","Radis, raifort" , 173., 173., 0.0968, 0.537, 0.441,  0.489, "#724899",
#' "090","Carotte"        , 152. , 152., 0.0850, 0.622, 0.537,  0.580, "#fff3b6",
#' "999","autres"         , 676. , 676., 0.378,  1,     0.622,  0.811, "#eeeeee"
#' )
#' 
graphique_surfaces_detail <- function(df, titre ="Répartition des surfaces", sous_titre ="") {
  if (length(na.omit(match(c("lib_classe", "surf_brut", "fraction","ymin","ymax"), names(df)))) < 5) {
    stop("graphique_surfaces_detail:ERROR: il manque des champs dans le df fourni", call. = FALSE) 
  }
  df %>%
    ggplot(aes(ymax = ymax, ymin = ymin, xmax = 4, xmin = 3, fill = lib_classe, width = 1)) +
    geom_rect(color = "white", linewidth = 1) +
    geom_text_repel(aes(x = 3.5, 
                        y = ylabel,
                        label = paste(format_entier(surf_brut),
                                      " ha; ",
                                      ifelse(round(fraction * 100) < 1,
                                             "< 1 %",
                                             round(fraction * 100)),
                                      "%"))) +
    geom_text(aes(label = "SAU", x = 2, y = 0)) +
    scale_fill_manual(values = df %>%
                                 pull(couleur) %>% 
                                 set_names(df$lib_classe),
                      limits = force) +
    coord_polar(theta = "y") +
    xlim(c(2, 4)) +
    labs(
      title = glue(titre),
      subtitle = sous_titre,
      fill = "",
      caption = glue("source : Agreste – recensement agricole {annee_ra_2020}")) +
    theme_void() +
    theme(legend.position = "bottom") + 
    guides(fill = guide_legend(nrow = 2, byrow = TRUE)) 
}
