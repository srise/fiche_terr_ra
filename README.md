# fiche_territoriale_ra

## Fiche territoriale Recensement agricole 2020

Générer une fiche d'indicateurs issus du Recensement agricole 2020 sur un territoire quelconque à partir d'une liste de codes INSEE de communes.


## Installation et utilisation

Voir le [guide utilisateur](documentation/guide_utilisateur.html).


## Support

assistance.fiches-territoriales-srise.draaf-grand-est@agriculture.gouv.fr
